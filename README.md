# ZX: The Zomp client
ZX is four things:

1. An end-user client for the Zomp package distribution system
2. A suite of code development and packaging tools in Erlang for Erlang
3. The back-end of Vapor, an Erlang GUI launcher and app browser
4. A runtime service for Erlang programs that need resources or notifications from Zomp

ZX and Zomp are developed with ZX and hosted with Zomp, so after initial installation keeping up to date is simple.

If you have ZX on your system, `zx run [program name]` will launch a program.

`zx create project` will start a project for you.

Read the [docs](http://zxq9.com/projects/zomp/) for more.

## Docs
[http://zxq9.com/projects/zomp/](http://zxq9.com/projects/zomp/)

## Code
[https://gitlab.com/zxq9/zx/](https://gitlab.com/zxq9/zx/)

# Installation

## TL;DR installation

### Linux/BSD/OSX/*nix:
1. Have an Erlang runtime installed.
2. Do `wget -q https://zxq9.com/projects/zomp/get_zx && bash get_zx`

### Windows:
Download the Windows installer from the [download page](https://zxq9.com/projects/zomp/download.en.html).

## Detailed information
For installation information see the [installation page](https://zxq9.com/projects/zomp/qs.install.en.html).

*NOTE: To roll a unix installation bundle from this repo run the `packup` script.  
It will generate `zx-[version].tar.xz` (Linux) and `zx-[version].tar.gz` (OSX, BSD, etc.) bundles.*
