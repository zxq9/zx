%%% @doc
%%% 〘*PROJECT NAME*〙 Client Service Supervisor
%%%
%%% This is the service-level supervisor of the system. It is the parent of both the
%%% client connection handlers and the client manager (which manages the client
%%% connection handlers). This is the child of 〘*PREFIX*〙_sup.
%%%
%%% See: http://erlang.org/doc/apps/kernel/application.html
%%% @end

-module(〘*PREFIX*〙_clients).
-vsn("〘*VERSION*〙").
-behavior(supervisor).
〘*AUTHOR*〙
〘*COPYRIGHT*〙
〘*LICENSE*〙

-export([start_link/0]).
-export([init/1]).


-spec start_link() -> {ok, pid()}.
%% @private
%% This supervisor's own start function.

start_link() ->
  supervisor:start_link({local, ?MODULE}, ?MODULE, none).

-spec init(none) -> {ok, {supervisor:sup_flags(), [supervisor:child_spec()]}}.
%% @private
%% The OTP init/1 function.

init(none) ->
    RestartStrategy = {rest_for_one, 1, 60},
    ClientMan = {〘*PREFIX*〙_client_man,
                 {〘*PREFIX*〙_client_man, start_link, []},
                 permanent,
                 5000,
                 worker,
                 [〘*PREFIX*〙_client_man]},
    ClientSup = {〘*PREFIX*〙_client_sup,
                 {〘*PREFIX*〙_client_sup, start_link, []},
                 permanent,
                 5000,
                 supervisor,
                 [〘*PREFIX*〙_client_sup]},
    Children  = [ClientSup, ClientMan],
    {ok, {RestartStrategy, Children}}.
