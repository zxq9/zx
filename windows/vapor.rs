// Prepare environment for Vapor via ZX and launch it using erl.exe

// Uncomment below to hide the console window.
#![windows_subsystem = "windows"]

use std::env;
use std::path::Path;
use std::path::PathBuf;
use std::string::String;
use std::ffi::OsString;
use std::fs::File;
use std::io::BufReader;
use std::io::prelude::*;
use std::process::Command;

fn main() {
    let zomp_dir = match env::var("ZOMP_DIR") {
        Ok(v) =>
            v,
        _ =>
            String::from(Path::new(&env::var("LOCALAPPDATA").unwrap())
                              .join("zomp")
                              .to_str()
                              .unwrap()),
    };
    env::set_var("ZOMP_DIR", OsString::from(&zomp_dir));

    let version_path = Path::new(&zomp_dir).join("etc").join("version.txt");
    let file = File::open(version_path);
    let mut reader = BufReader::new(file.unwrap());
    let mut version = String::new();
    reader.read_line(&mut version).unwrap();
    version = version.trim().to_string();
    env::set_var("ZX_VERSION", OsString::from(&version));

    let zx_dir = Path::new(&zomp_dir)
                      .join("lib")
                      .join("otpr")
                      .join("zx")
                      .join(version);
    env::set_var("ZX_DIR", zx_dir.to_str().unwrap());

    let env_pf = env::var("PROGRAMFILES").unwrap();
    let pf = Path::new(&env_pf);
    let mut maj: i32 = 0;
    let mut min: i32 = 0;
    let mut erl_ver = String::new();
    let mut best_path = PathBuf::new();
    for entry in std::fs::read_dir(pf).unwrap() {
        let path = entry.unwrap().path();
        let installation = path.file_name().unwrap().to_str().unwrap();
        if installation.starts_with("erl") {
            if installation.starts_with("erl-") {
                erl_ver = installation.trim_start_matches("erl-").to_string();
            } else {
                erl_ver = installation.trim_start_matches("erl").to_string();
            }
            let parts: Vec<&str> = erl_ver.split('.').collect();
            let m_maj: i32 = parts[0].parse().unwrap();
            let m_min: i32 = parts[1].parse().unwrap();
            if m_maj > maj {
                maj = m_maj;
                min = m_min;
                best_path.push(&path);
            } else if m_maj == maj {
                if m_min > min {
                    min = m_min;
                    best_path.push(&path);
                }
            }
        }
    }
    let erl = Path::new(&best_path).join("bin").join("erl.exe");
    let escript = Path::new(&best_path).join("bin").join("escript.exe");

    let zx_ebin = Path::new(&zx_dir).join("ebin");
    let zx_beam = Path::new(&zx_ebin).join("zx.beam");
    if ! zx_beam.exists() {
        println!("Rebuilding ZX. Just a moment...");
        let pwd = env::current_dir().unwrap();
        assert!(env::set_current_dir(zx_dir).is_ok());
        Command::new(escript)
                .arg("make_zx")
                .output()
                .expect("Failed to execute make_zx escript.");
        assert!(env::set_current_dir(pwd).is_ok());
    }

    Command::new(erl)
            .arg("-noshell")
            .arg("-pa")
            .arg(zx_ebin.to_str().unwrap())
            .arg("-run")
            .arg("zx")
            .arg("do")
            .arg("-extra")
            .arg("run")
            .arg("vapor")
            .spawn()
            .expect("Failed to start Vapor.");
}
