// Prepare environment for ZX and launch it using werl.exe

use std::env;
use std::path::Path;
use std::path::PathBuf;
use std::string::String;
use std::ffi::OsString;
use std::fs::File;
use std::io::BufReader;
use std::io::prelude::*;
use std::process::Command;

fn main() {
    let args: Vec<String> = env::args().collect();

    let zomp_dir = match env::var("ZOMP_DIR") {
        Ok(v) =>
            v,
        _ =>
            String::from(Path::new(&env::var("LOCALAPPDATA").unwrap())
                              .join("zomp")
                              .to_str()
                              .unwrap()),
    };
    env::set_var("ZOMP_DIR", OsString::from(&zomp_dir));
    println!("ZOMP_DIR: {}", zomp_dir);

    let version_path = Path::new(&zomp_dir).join("etc").join("version.txt");
    let file = File::open(version_path);
    let mut reader = BufReader::new(file.unwrap());
    let mut version = String::new();
    reader.read_line(&mut version).unwrap();
    version = version.trim().to_string();
    env::set_var("ZX_VERSION", OsString::from(&version));
    println!("ZX_VERSION: {}", version);

    let zx_dir = Path::new(&zomp_dir)
                      .join("lib")
                      .join("otpr")
                      .join("zx")
                      .join(version);
    env::set_var("ZX_DIR", zx_dir.to_str().unwrap());
    println!("ZX_DIR: {}", zx_dir.to_str().unwrap());

    let env_pf = env::var("PROGRAMFILES").unwrap();
    let pf = Path::new(&env_pf);
    let mut maj: i32 = 0;
    let mut min: i32 = 0;
    let mut erl_ver = String::new();
    let mut best_path = PathBuf::new();
    for entry in std::fs::read_dir(pf).unwrap() {
        let path = entry.unwrap().path();
        let installation = path.file_name().unwrap().to_str().unwrap();
        if installation.starts_with("erl") {
            if installation.starts_with("erl-") {
                erl_ver = installation.trim_start_matches("erl-").to_string();
            } else {
                erl_ver = installation.trim_start_matches("erl").to_string();
            }
            let parts: Vec<&str> = erl_ver.split('.').collect();
            let m_maj: i32 = parts[0].parse().unwrap();
            let m_min: i32 = parts[1].parse().unwrap();
            if m_maj > maj {
                maj = m_maj;
                min = m_min;
                best_path.push(&path);
            } else if m_maj == maj {
                if m_min > min {
                    min = m_min;
                    best_path.push(&path);
                }
            }
        }
    }
    let werl = Path::new(&best_path).join("bin").join("werl.exe");
    println!("werl.exe path: {}", werl.to_str().unwrap());

    let escript = Path::new(&best_path).join("bin").join("escript.exe");
    println!("escript.exe path: {}", escript.to_str().unwrap());

    let zx_ebin = Path::new(&zx_dir).join("ebin");
    let zx_beam = Path::new(&zx_ebin).join("zx.beam");
    println!("zx.beam path: {}", zx_beam.to_str().unwrap());
    if ! zx_beam.exists() {
        println!("Rebuilding ZX. Just a moment...");
        let pwd = env::current_dir().unwrap();
        assert!(env::set_current_dir(zx_dir).is_ok());
        Command::new(escript)
                .arg("make_zx")
                .output()
                .expect("Failed to execute make_zx escript.");
        assert!(env::set_current_dir(pwd).is_ok());
    } else {
        println!("ZX found. Running.");
    }

    Command::new(werl)
            .arg("-pa")
            .arg(zx_ebin.to_str().unwrap())
            .arg("-run")
            .arg("zx")
            .arg("do")
            .arg("-extra")
            .args(&args[1..])
            .spawn()
            .expect("Failed to start ZX.");
}
