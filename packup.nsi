Name "Vapor"
OutFile "../InstallVapor.exe"
InstallDir "$LOCALAPPDATA\zomp"

# LoadLanguageFile "${NSISDIR}\Contrib\Language Files\Japanese.nlf"

Section
  SetOutPath "$INSTDIR"
  File "README.md"
  File "LICENSE"
  File /r "zomp\etc"
  File /r "zomp\key"
  File /r "zomp\lib"
  File "windows\zx.exe"
  File "windows\zxh.exe"
  File "windows\vapor.exe"

  CreateDirectory "$SMPROGRAMS\Vapor"
  CreateShortcut "$SMPROGRAMS\Vapor\Vapor.lnk" "$INSTDIR\vapor.exe" ""
  CreateShortcut "$SMPROGRAMS\Vapor\Uninstall.lnk" "$INSTDIR\Uninstall.exe" ""
  CreateShortcut "$DESKTOP\Vapor.lnk" "$INSTDIR\vapor.exe" ""
  CreateShortcut "$PROFILE\zx.lnk" "$INSTDIR\zx.exe" ""
  CreateShortcut "$PROFILE\zxh.lnk" "$INSTDIR\zxh.exe" ""

  WriteUninstaller "$INSTDIR\Uninstall.exe"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Vapor" "DisplayName" "Vapor"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Vapor" "UninstallString" '"$INSTDIR\Uninstall.exe"'
SectionEnd

Section "Uninstall"
  RMDir /r "$SMPROGRAMS\Vapor"
  Delete "$DESKTOP\Vapor.lnk"
  Delete "$PROFILE\zx.lnk"
  Delete "$PROFILE\zxh.lnk"
  RMDir /r "$INSTDIR"
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Vapor"
SectionEnd
